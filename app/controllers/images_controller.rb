class ImagesController < ApplicationController
  before_action :authenticate_user!
  
  def index
    @profile  = Profile.find current_user.id 
    @images = @profile.images

   
  end

  def show
  end

 

  def update
  end

  def new
    @profile = Profile.find current_user.id
    @image = @profile.images.build

  end

  def create
    @profile = Profile.find current_user.id
    @image = @profile.images.build (image_params)
    @image.save
    redirect_to images_path
  end

  def edit
  end

  def delete
  end

  private 
  def image_params 
    params.require(:image).permit(:subtitle,:file,:public,:image)
  end
  
end
