Rails.application.routes.draw do
  devise_for :users, controllers:{
    registrations: 'users/registrations'
  }
  resources :mugs
  resources :profiles
  resources :images
  root :to => 'mugs#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
