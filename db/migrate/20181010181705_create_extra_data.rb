class CreateExtraData < ActiveRecord::Migration[5.2]
  def change
    create_table :extra_data do |t|
      t.string :name
      t.string :occupation
      t.string :message
      t.references :mug
      t.references :hug

      t.timestamps
    end
  end
end
